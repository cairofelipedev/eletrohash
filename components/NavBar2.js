import React from 'react'
import OpenClick from '../components/OnClick.js'
import CloseClick from '../components/CloseClick.js'
import {
  NavLink
} from 'reactstrap';

export default function NavBar2() {
  return (
    <>
      <header id="header">
      <OpenClick/>
        <div className="container d-flex align-items-center">
          <h1 className="logo mr-auto"><a href="index.html">Eletro#Hash</a></h1>
            <nav className="nav-menu d-none d-lg-block">
              <ul>
                <li className="active">
                  <NavLink href="#">Categorias</NavLink>
                </li>
                <li>
                  <NavLink href="#">Clientes</NavLink>
                </li>
                <li>
                  <NavLink href="#">Sobre</NavLink>
                </li>
                <li>
                  <NavLink href="#">Contato</NavLink>
                </li>
              </ul>
            </nav>
          </div>
      </header>
      <div id="myNav" className="container-fluid menu-nav">
        <div className="menu-content">
        <CloseClick/>
            <li className="active">
              <NavLink href="#">Categorias</NavLink>
            </li>
            <li>
              <NavLink href="#">Clientes</NavLink>
            </li>
            <li>
              <NavLink href="#">Sobre</NavLink>
            </li>
        </div>
      </div>
    </>

    )
}