import React from 'react';

function CloseClick() {

  function CloseNav() {
    document.getElementById("myNav").style.height = "0%";
  }
  
  return (
    <a className="closebtn" onClick={CloseNav}>&times;</a>
  );
}

export default CloseClick;