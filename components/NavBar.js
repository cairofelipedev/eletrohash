import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">EletroHash</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
          <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Categorias
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  Notebooks
                </DropdownItem>
                <DropdownItem>
                  Computadores
                </DropdownItem>
                <DropdownItem>
                  Celulares
                </DropdownItem>
                <DropdownItem>
                  Gamer
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  Todos
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <NavItem>
              <NavLink href="/components/">Clientes</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">Sobre</NavLink>
            </NavItem>
          </Nav>
          <NavbarText>Contato</NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;