import React from 'react';

function OnClick() {

  function sayHello() {
    document.getElementById("myNav").style.height = "100%";
  }
  
  return (
    <span className="menu-icon" onClick={sayHello}>
      &#9776;
    </span>
  );
}

export default OnClick;