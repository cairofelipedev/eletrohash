import Head from 'next/head'
import NavBar2 from '../components/NavBar2'
export default function Home() {
  return (
    <>
      <Head>
        <title>Eletro Hash - Encontre o computador ideal para você</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar2/>
    </>
  )
}
